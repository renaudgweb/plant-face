g = Graphics.createArrayBuffer(5,5,1);

function drawFaces() {
  setInterval(function() {
    var soil = analogRead(D1).toFixed(2);
    if (soil <= 0.15) {
      g.clear();
      show("     \n"+
           "1   1\n"+
           "     \n"+
           " 111 \n"+
           "1   1\n");
    } else if (soil <= 0.30) {
        g.clear();
        show("     \n"+
             "1   1\n"+
             "     \n"+
             "11111\n"+
             "     \n");
    } else {
        g.clear();
        show(" 1 1 \n"+
             "11111\n"+
             "11111\n"+
             " 111 \n"+
             "  1  \n");
    }
  }, 1000);
}

drawFaces();

setWatch(function() {
  clearInterval();
  g.clear();
  drawFaces();
}, BTN1+BTN2, {repeat:true, debounce:20, edge:"falling"});